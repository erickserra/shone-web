import { theme as defaultTheme } from '@chakra-ui/core'

const theme = {
	...defaultTheme,
	fonts: {
		body: 'Roboto, system-ui, sans-serif',
		heading: 'Roboto, system-ui, sans-serif',
		mono: 'Menlo, monospace'
	},
	colors: {
		...defaultTheme.colors,
		primary: {
			500: '#8257e5'
		}
	}
}

export default theme
